#!/bin/env bash

# set -x
set -e

readonly PROGNAME="$0"

function log() {
    local msg="$@"
    if [ -n "${msg}" ]; then
        echo "${msg}" >&2
    fi
}

function die() {
    log "$@"
    exit -1
}

function usage() {
    log "${PROGNAME} docname-revnum"
    log "${PROGNAME} \"conflict-review-\"remainder orig_docname-revnum"
    die
}

function strip_trailing_revnum() {
    echo "$1" | sed -re 's/-[[:digit:]]+$//'
}

readonly DOCS_DIR='./docs'
if [ ! -d "${DOCS_DIR}" ]; then
    die "MUST run this command from a directory containing '${DOCS_DIR}'"
fi

readonly TEMPLATE='./template.txt'
if [ ! -f "${TEMPLATE}" -o ! -r "${TEMPLATE}" ]; then
    die "MUST run this command from a directory containing '${TEMPLATE}'"
fi

if [ $# -lt 1 ]; then
    usage
fi

readonly docname_revnum="$1"

docname_base=$(strip_trailing_revnum ${docname_revnum})
if [ "${docname_revnum}" = "${docname_base}" ]; then
    usage
fi

# Special-case charters ("charter-ietf-foo-03-01") and conflict reviews
# (which need to have the conflict referent inserted into the template).
case "${docname_revnum}" in
    charter-*)
        docname_base=$(strip_trailing_revnum ${docname_base})
        ;;
    conflict-review-*)
        # TODO(ek): implement
        ;;
esac

echo "${docname_revnum} -> ${docname_base}"

readonly docname_dir="$DOCS_DIR/${docname_base}"
if [ ! -d "${docname_dir}" ]; then
    log "mkdir '${docname_dir}'"
    mkdir "${docname_dir}"
fi

readonly docname_revnum_file="${docname_dir}/${docname_revnum}.txt"
if [ ! -f "${docname_revnum_file}" ]; then
    log "preparing '${docname_revnum_file}'"
    sed -e 's/doc[{][}]/doc{'"${docname_revnum}"'}/'  \
        -e 's/[{]doc-rev[}]/'"${docname_revnum}"'/'   \
        "${TEMPLATE}" > "${docname_revnum_file}"
else
    log "found '${docname_revnum_file}'"
fi

