doc{draft-ietf-alto-path-vector-19}
ballot{No Objection}


[ document review ]

[[ comments ]]

[S1; question]

* I assume that all of this querying is inherently asymmetric?

  By that I mean that if an app wants to truly estimate bidirectional flow
  usage between "a" and "b" it needs to query for "a->b" and "b->a", in case
  there's some fundamental asymmetry in flow characteristics.

[S4.2.1; nit]

* s/QEB/QEP/g
