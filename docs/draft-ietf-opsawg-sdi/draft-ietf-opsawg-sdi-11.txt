doc{draft-ietf-opsawg-sdi-11}
ballot{No Objection}

[[ comments ]]

* Perhaps a suggestion that vendors might want to have a factory-installed
  option for interested customers that /only/ an encrypted config can be tried
  and no attempt to use a plaintext config will be made.

* Security considerations paragraph about control of the configuration server
  should include a mention that the key is not required for interference if
  the booting device will happily fall back to loading a cleartext config.

* Though less common than DHCPv4, consider acknowledging the broader (open)
  issue of file/TFTP server service discovery (DHCPv6, RAs plus resolution of
  a well-known hostname, DNSSD, ...).


[[ nits ]]

[ section 4.2 ]
* "Publish TFTP Server" --> "Publish to TFTP Server", perhaps
