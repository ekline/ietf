doc{draft-ietf-dnsop-caching-resolution-failures-07}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-dnsop-caching-resolution-failures-07
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Nits

### S1.2

* "only exacerbated" -> "further exacerbated"?

  Use of "only" here might be misread.

### S2.2

* s/2001:DB8:1::/2001:db8:1::/g

  in accordance with RFC 5952 section 4.3
