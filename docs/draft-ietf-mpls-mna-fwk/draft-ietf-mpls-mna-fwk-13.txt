doc{draft-ietf-mpls-mna-fwk-13}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-mpls-mna-fwk-13
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Comments

### S2.2

* If text in 3031 this refers to is Section 3.18 then I think stating as
  much might be helpful.
