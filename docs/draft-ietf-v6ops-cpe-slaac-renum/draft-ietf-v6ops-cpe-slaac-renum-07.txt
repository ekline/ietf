doc{draft-ietf-v6ops-cpe-slaac-renum-07}
ballot{Yes}


[[ comments ]]

[ section 3.4 ]

* At the end of the 2nd paragraph and the 2nd NOTES paragraph, I think it
  could be made clearer that this recommendation especially applies to ND
  Options that contain address or prefix information from within a prefix
  learned on the WAN side.

  These timers seem fine in general as well, but I think as written it's
  a blanket recommendation without explicitly saying why these options
  need updating, i.e. if someone asks what does "if and where applicable"
  actually mean -- it means ND options with address/prefix information
  taken from a delegated prefix whose lifetime has been reduced
  (possibly to zero, possibly progressively shorter values trending to zero).
