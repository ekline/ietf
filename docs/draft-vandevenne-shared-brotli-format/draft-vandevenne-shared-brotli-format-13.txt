doc{draft-vandevenne-shared-brotli-format-13}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-vandevenne-shared-brotli-format-13
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Comments

### S8.2, S8.4.3, S9

* It would be good have a reference for "highwayhash" in this document.

## Nits

### S1.1

* "format format" -> "format"

### S4

* "one ore more" -> "one or more"
