doc{draft-ietf-pce-pcep-extension-for-pce-controller-12}
ballot{Discuss}


[[ discuss ]]

[ section 7.3.1 ]

* This intended handling of the LINKLOCAL-IPV6-ID-ADDRESS TLV does not seem
  to be discussed anywhere in this document.  Should there be some text
  about it, or is this TLV left over from previous iterations of the document?

* Saying that the LINKLOCAL-IPV6-ID-ADDRESS TLV holds a pair of global IPv6
  addresses seems confusing to me.

  If the pair of global IPv6 addresses is to be considered "on link" in the
  sense that IPv6 ND can be successfully be performed on the link for both
  of these addresses, then "ONLINK" might be better than LINKLOCAL.

* Also, why are two interface IDs required?  I would have expected that only
  the outgoing interface name/ID would be of relevance to the recipient of
  a message with TLV in it?


[[ nits ]]

[ section 4 ]

* "provide a mean" -> "provide a means", perhaps

[ section 5.5.4 ]

* "and then notify it to" -> "and then sends a notification to", or something
