doc{draft-zern-webp-14}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-zern-webp-14
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Nits

### S2.7

* There's a 'VP8' in paragraph with a run of chuck types that could perhaps
  be 'VP8 ' to match the explicit space character style used elsewhere in
  the doc?
