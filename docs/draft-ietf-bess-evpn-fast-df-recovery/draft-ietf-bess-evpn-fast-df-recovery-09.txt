doc{draft-ietf-bess-evpn-fast-df-recovery-09}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-bess-evpn-fast-df-recovery-09
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Comments

### S2

* Where is there operational guidance to PE implementors that "recovery"
  activity SHOULD NOT be initiated until after the configured time
  synchronization operations (e.g. NTP) have occurred?

  I didn't see anything in 7432 nor in 8584, but I might not be looking in
  the right places or for the right keywords.
