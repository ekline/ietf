doc{charter-ietf-keytrans-00-11}
ballot{No Objection}


[ document review ]

# Internet AD comments for charter-ietf-keytrans-00-11
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

## Nits

### Program of Work

* Specifying -> Specify, and
  Standardizing -> Standardize

  might read more naturally
