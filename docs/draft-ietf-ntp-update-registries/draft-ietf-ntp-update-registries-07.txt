doc{draft-ietf-ntp-update-registries-07}


[ document review ]

# Internet AD comments for draft-ietf-ntp-update-registries-07
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

## Comments

### Abstract,S1

* s/reviews/reviews, and updates where necessary,/

  ...or something, because the document does more than just review the
  registries.

### S2.1

* The last paragraph about Extension Fields seems like it belongs at the
  head of S2.2 (it doesn't align with the title of S2.1, thought does align
  with all of S2.1 being about "things that came from 5905").

  Suggest: moving this paragraph to the start of S2.2 and replacing
  "did not create a registry for it" with "did not create a registry for the
  Extension Field Type field", or something to clarify the referent of "it".

### S3

* Just to clarify, specifically as applies to the registries in S4.1 and
  S4.2, that 4-octet codes beginning with "X" may now be handed out by IANA,
  at least in principle?

  If so, it might be good to explicitly acknowledge this and say that it's
  not expected to

### S3,S4+

* Since DEs are being asked for, we'll be asked to have a section outlining
  guidance to the DEs.

  Cribbing from some of the recommendations mentioned in RFC 8126, here's a
  stab at some text to get started:

  S3.1 Guidance to Designated Experts

  In all cases of review by the Designated Expert (DE) described here,
  the DE is expected to ascertain the existence of suitable
  documentation (a specification) as described in [RFC5226] and to
  verify that the document is permanently and publicly available.  The
  DE is also expected to check the clarity of purpose and use of the
  requested code points.  Last, the DE is expected to be familiar with this
  document, specifically the history documented here.  If reviewing a request
  to allocate a field value previously reserved for private or experimental
  use, but reallocated per this document, it is RECOMMENDED to seek the
  feedback of the NTP community via any of the regular working group
  participation mechanisms (specifically, but not limited to, inquiring on
  the mailing list).

  ... or something.
