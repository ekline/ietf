doc{draft-ietf-homenet-naming-architecture-dhc-options-22}
ballot{No Objection}


[ document review ]

# Internet AD comments for {draft-ietf-homenet-naming-architecture-dhc-options-22}
CC @ekline

## Comments

* I wonder if it's possible to include some mention that when a prefix
  (IA_PD) is changed and the OPTION_REVERSE_DIST_MANAGER has changed (perhaps
  because a region is being split into smaller units and the infrastructure
  is shifting) that the OPTION_REVERSE_DIST_MANAGER option SHOULD be sent
  along with the new IA_PD.
