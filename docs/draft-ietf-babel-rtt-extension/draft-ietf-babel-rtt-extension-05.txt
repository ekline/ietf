doc{draft-ietf-babel-rtt-extension-05}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-babel-rtt-extension-05
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Comments

### S3.2

* Up to you, but it occurs to me that in this final paragraph you might also
  note that any routers using NTP can manage their clock drift, further
  minimizing the likelihood of impacting the RTT calculation described here.

### S4.1

* Even within an Experimental subsection within a Standards Track document,
  itself a bit unusual, it seems odd to have text like "our implementation"
  and "we have/have not ...".  Can this be reworded to avoid giving the
  impression that there's an IETF-approved implementation?  Perhaps something
  like "At least one implementation", "One or more implementations are known
  to ...", or something?

## Nits

### S1

* "this kind of link layers" -> "these kinds of link layers"?
