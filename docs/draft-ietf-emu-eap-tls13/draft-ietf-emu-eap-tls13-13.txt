doc{draft-ietf-emu-eap-tls13-13}
ballot{No Objection}


[[ nits ]]

[ section 1 ]

* s/and and/and/

[ section 2.1.7 ]

* "Many client certificates contains" -> "Many client certificates contain"

[ section 5.4 ]

* s/EAP--TLS/EAP-TLS/

[ section 5.9 ]

* "In the context EAP-TLS" -> "In the context of EAP-TLS"?
