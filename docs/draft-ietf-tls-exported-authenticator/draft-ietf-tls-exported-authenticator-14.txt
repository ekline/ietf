doc{draft-ietf-tls-exported-authenticator-14}
ballot{No Objection}


[[ comments / questions / nits ]]

[ sections 4 & 5 ]

* "SHOULD use a secure with" ->
  "SHOULD use a secure communications channel with" or
  "SHOULD use a secure transport with" or something?

* "as its as its" -> "as its"

[ section 5.2.3 ]

* I think the first sentence of the first paragraph may not be a
  grammatically complete sentence?

[ section 7 ]

* "following sections describes APIs" ->
  "following sections describe APIs"
