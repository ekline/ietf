doc{draft-ietf-bmwg-evpntest-09}
ballot{Discuss}


[[ discuss ]]

[S3.9]

* I think this should probably be easy to clear up, likely by clarifying
  the scope of the test.  The general point I'd like to discuss is that ND
  machinery has many more test cases than ARP REPLYs do, and it would be
  good to clarify what is to be tested.  For that matter, ARP has fun cases
  too.

  The test in S3.9 seems to be for L2 unicast traffic ("[s]end ... to the
  target IRB address"), but should there be tests for learning in other
  scenarios?

    (1) L2 broadcast Gratuitous ARP REQUESTs (GARP)?

    (2) L3 Multicast NAs to all-nodes (Solicited flag = 0)

    (3) L3 Multicast NAs to all-routers (Solicited flag = 0)

  My guess is it's simplest to craft text to just say the test covers
  unicast ARP REPLYs and unicast ND NAs, but the option exists to expand
  the test matrix as well, I suppose.


[[ comments ]]

[S3.3] [question]

* "Fail the DUT-CE link" means to cause a link failure that can be detected
  by the DUT (e.g., a lost of PHY signal or some means to detect a
  directly-connected cable was removed)?

[S3.9] [question]

* What does "Send X arp/neighbour discovery(ND)" mean, exactly?

  If these are meant to be ARP REPLY/Neighbor Advertisements (NAs) then
  being explicit about that seems helpful.

  Or are these ARP REQUESTs and NS Neighbor Solicitations for the IRB address?

* "Send ... to the target IRB address configured in EVPN instance" seems
  like the ARP REPLY/ND NAs are to be sent unicast.  If that's true, I
  think it would improve clarity to say they're unicast.
