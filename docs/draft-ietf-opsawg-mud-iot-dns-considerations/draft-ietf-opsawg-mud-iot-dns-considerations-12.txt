doc{draft-ietf-opsawg-mud-iot-dns-considerations-12}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-opsawg-mud-iot-dns-considerations-12
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Comments

### S3

* "ip6.arpa", not "ipv6.arpa"

  This is correct elsewhere in the doc, but this seems to have been missed.

### S3.2

* "recursive servers should cache data for at least..."

  ... while still respecting TTLs in the replies, yes?

### S6.4

* I suggest finding some text to point to that defines what a "geofenced"
  name is.  Right now this feels like the kind of thing that everyone
  "just knows what it means", but could use some formal description.

## Nits

### S3.1

* s/mapping/mappings/?

### S4.1

* s/inprotocol/in-protocol/

### S4.2

* "all those addresses DNS for the the name" ->
  "all those addresses in the DNS for the name"
