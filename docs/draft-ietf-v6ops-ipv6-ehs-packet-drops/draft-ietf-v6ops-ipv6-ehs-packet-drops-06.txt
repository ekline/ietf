doc{draft-ietf-v6ops-ipv6-ehs-packet-drops-06}
ballot{Yes}


[[ comments ]]

[ section 7.1 ]

* It seems to me that this also applies to IP fragments of any family,
  particularly if the first fragment is delayed or lost.

  I can't tell from the text whether handling fragments (even in IPv4)
  is considered "process[ing] the packet outside the normal forwarding path"
  or not.


[[ nits ]]

[ section 3 ]

* s/IPv6 IPv6/IPv6/?

[ section 6.1 ]

* "the router on the sending side of the link needs to..."?

  This phrasing seems a little odd to me.  How about just
  "the (or a) forwarding router needs to...", since the rest of the sentence
  does make the described situation clear?
