doc{draft-ietf-regext-secure-authinfo-transfer-06}
ballot{No Objection}


[[ questions ]]

[ section 4.1 ]

* How does "the age of [RFC4086]" factor in to the calculation that yielded
  the "at least 128 bits of entropy" determination?  Is there an update
  to RFC 4086 that should be underway?


[[ nits ]]

[ section 4.3 ]

* "an NULL": consider "a NULL", matching section 4.4
