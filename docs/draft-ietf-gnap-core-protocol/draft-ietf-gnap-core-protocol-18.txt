doc{draft-ietf-gnap-core-protocol-18}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-gnap-core-protocol-18
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Nits

### S1.2

* "which the absolute" -> "which is the absolute", I think

### S13.9

* "able capture of" -> "able to capture", I think
