doc{draft-ietf-lpwan-schc-over-nbiot-12}
ballot{No Objection}


[ document review ]

# Internet AD comments for {draft-ietf-lpwan-schc-over-nbiot-12}
CC @ekline

## Comments

### S5.3

* What exactly is meant here?

                                                     ...  The IPv6 and
  IPv4 deployment may force to get more rules to deal with each case.

  Does this just mean that dual-stack deployments require more rules than
  either IPv6-only or IPv4-only deployments?

### Appendix B

* "16000 bytes"

  Is this 16000 correct?  Or should it be 1600?

## Nits

### S5.4.2

* "COULD" is not in the 8174 list of meaningfully capitalized words.  Even
  attempting to interpret as a "MAY" doesn't really apply in this wording.

  Suggestion: lowercase is fine.

### Appendix A.2

* Perhaps: s/octets aligned/octet-aligned/

### Appendix A.3

* "HARQ" isn't defined until Appendix B.  Consider fully expanding the
  initialism here.
