doc{draft-ietf-dnsop-no-response-issue-18}
ballot{Yes}

[nits]

S3.2.2

* s/answers responses/responses/  (or answers)

S5

* Is there a reference for a definition of "scrubbing service"?

S10

* s/None the tests/None of the tests/
