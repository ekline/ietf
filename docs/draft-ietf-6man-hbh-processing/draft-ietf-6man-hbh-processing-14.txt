doc{draft-ietf-6man-hbh-processing-14}


[ document review ]

# Internet AD comments for draft-ietf-6man-hbh-processing-14
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Discuss

### S5.1

* I'm having a hard time understanding how:

  - this section has normative language about process EHs,
  - this section says draft-eh-limits contains further requirements
  - draft-eh-limits is presently NOT a normative dependency

  I know we might not want to tie this document to draft-eh-limits, but in
  order to make that argument I kinda feel like it'll be necessary to
  clarify the difference.

  Given a quick scan of draft-eh-limits, which is listed as a BCP, I think
  it suffices to change "further requirements" to something like
  "additional recommendations"? I think that might underscore that
  draft-eh-limits is all SHOULD (and we probably need to clarify that the
  MUSTs in draft-eh-limits apply to nodes complying with the BCP).

  Thoughts?

### S6

* I don't know what it would mean to a specification author that a "[n]ew
  Hop-by-Hop [option] SHOULD be designed expecting that a router may drop
  packets containing the new Hop-by-Hop option".

  I think it's the intend *use* of the option that should be designed to
  understand it may be dropped, rather than the option itself?

## Comments

### S5.2

* "which can be mitigated when using a reverse path forwarding (RPF) check"

  My gut reaction is that this might be more specifically written as "which
  can be mitigated to varying degrees by using a reverse path forwarding
  (RPF) check."

  This is because it depends upon where along the path the first the uRPF
  check is done.  A spoofing source might be able to DoS a ~neighbor within
  the "zone of uRPF" (blast radius) from the triggered router's perspective.

## Nits

### S4

* "many types network path" -> "many types of network paths"

* "into the the processor" -> "into the processor"

* "could cause adversely impact router operation"
  "could adversely impact router operation" or
  "could cause adverse impact router operation"
