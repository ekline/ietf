doc{draft-ietf-pim-igmp-mld-snooping-yang-15}
ballot{No Objection}

[ questions ]

* What's the origin for using rt-types:timer-value-seconds16 for the group
  and source expire value?  I spent some time with 2710 and 3810 looking
  for where this might have come from, but perhaps I missed something.
