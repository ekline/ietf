doc{charter-ietf-srv6ops-00-03}
ballot{No Objection}


[ document review ]

# Internet AD comments for charter-ietf-srv6ops-00-03
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Comments

* It's nice that it's scoped to Informational documents, but should we leave
  open the possibility that an operations group might want to produce a BCP?
