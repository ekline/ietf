doc{draft-ietf-dnssd-srp-23}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-dnssd-srp-23
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Comments

### S3.1.1

* "discover the server to which they should send SRP updates"

  What about servers, plural?  Can there reasonably be more than one
  registrar, and if so does a client just send updates to all of them
  (and, further, how does it handle a partial failure issuing an update)?

### S6.1

* When checking an update for its on-link property, consider whether
  recommending RFC 5082 "The Generalized TTL Security Mechanism (GTSM)"
  might be appropriate.

### S10.6

* How should DNS-over-TLS work in this scenario?  Should the certificate
  claim to be valid for this address, or for default.service.arpa.,
  or something?
