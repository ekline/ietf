
[ nits ]

- in general

    - Several links that are meant to be other_rfc#section_X.Y.Z render
      instead as this_document#section_X.Y.Z (in the tools.ietf.org
      rendering).

- Abstract

    ? "several places of a network" -->
      "several types of networks"
    ? "procedural mitigations techniques" -->
      "procedural mitigation techniques"

- It's not clear if RFC 2119 text is needed for this document as it is now.

- 2.1

    ? "abundance of address space available" -->
      "abundance of address space is available"

- 2.1.5

    - Could perhaps more explicitly state that DHCPv6 is not mandatory
      to implement per IPv6 Node Requirements (RFC 8504).

- 2.1.6

    ? "are specific consideration" -->
      "are specific considerations"

- 2.2

    - One might quibble with the statement "the extension header chain
      must be be parsed completely".  It has to be parsed enough so that
      it can be completely traversed, but it need not necessarily be
      parsed in a way that a node has to "understand" the contents --
      this is how the extension headers are designed, after all.

      Regrettably, no better wording comes to mind, so I have no specific
      recommendation for what could be done here.

- 2.3.2

    ? "either intentional or malicious" -->
      "either unintentional or malicios" (not quite sure)

    - This section could have a callback to 2.1.7 as a possible solution
      (toward the end, where it talks about host isolation) as this can
      also solve these problems.  (A forward link to 2.3.4 might be good
      too, since this is philosophically similar.)

- 2.4

    ? "number of Dijsktra execution" -->
      "number of Dijsktra executions"

- 2.4.1

    ? "configured such as" -->
      "configured so as to"

- 2.4.2

    - With the mention of NTP I suddenly thought: should there be
      DNS-related text as well, or does that fall within this section too?

- 2.4.3

    ? "Both the save" -->
      "Both to spare"

- 2.5.3

    - The CYMRU link doesn't seem to go to a useful page anymore.  :-/

- 2.6

    - SNMP is mentioned (eslewhere too).  Should YANG/NETCONF/RESTCONF
      also get a gratuitous reference?

    - Same question for DIAMETER vis a vis RADIUS.

- 2.6.1.5

    ? "operation security" -->
      "operational security"

- 2.6.2.2

    ? "in some case" -->
      "in some cases"

    ? "can sometime be" -->
      "can sometimes be"

- 2.6.2.3

    - Even though section 2.6.1.1 already references RFC 5952 as the
      current recommended canonical string format, this section could
      link to it as well (just in case a reader has followed a deep link
      into this section and hasn't read anything else).

- 2.7.1

    - Perhaps "you have twice" --> "the network operator has twice".

    ? "more relax security" -->
      "more relaxed security"

- 2.7.2

    ? "no more automated in most environment" -->
      "no longer automatic in most environments"

- 2.7.2.7

    ? "is no more used by" -->
      "is no longer used by"

- 2.7.2.8

    - If UDP filtering guidelines are to be listed here (even
      parenthetically), you might include UDP 443 for QUIC, 500 for IKE,
      and 3478 for STUN.  Maybe just replace "block all" with something
      like "filter all judiciously" or something.

    ? "no more enabled" -->
      "no longer enabled"

- 2.7.3.1

    ? "and effective" -->
      "an effective"

- 3.2

    ? "IPv6-in-IP4" -->
      "IPv6-in-IPv4"

    - There appears to be a broken internal reference to, I presume,
      section 2.8?

    ? "using IP4" -->
      "using IPv4"

    - Since this section mentions filtering at the Internet connection,
      should it also have a reference to BCPs 38 and 84, for good measure?
      It is slightly different, so you might deem it unrelated.

- 4.2

    ? "coexistence i" -->
      "coexistence section"

- 4.3

    ? "powers up" -->
      "establishes a data connection" maybe?

- 5

    ? "have all IPv6 enabled" -->
      "all have IPv6 enabled"

- 6

    ? "for your convenience" -->
      "for the reader's convenience" maybe?
