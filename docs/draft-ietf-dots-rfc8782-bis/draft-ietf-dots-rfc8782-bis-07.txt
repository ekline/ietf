doc{draft-ietf-dots-rfc8782-bis-07}
ballot{No Objection}


[S3] [comment]

* I don't know if it's really necessary to dredge up RFC 6296, but I
  understand the desire for completeness.

[S4.4.1.1] [question]

* For "lifetime" in the case where a "target-fqdn" was given, should the
  resolution library's knowledge of the DNS RR TTL value(s) be factored in?

  For example, what does lifetime=3600s mean for a hostname whose A/AAAA
  RRs have only 5 minute lifetimes?  Is the DOTS server/enforcer expected
  to continuously re-resolve every ${DNS_RR_TTL} and apply the policy for
  up to the full 3600 seconds, or is the DNS RR TTL ignored once the
  resolution has been confirmed to have succeeded or failed?
