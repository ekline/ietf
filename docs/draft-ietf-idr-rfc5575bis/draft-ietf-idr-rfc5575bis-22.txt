doc{draft-ietf-idr-rfc5575bis-22}
ballot{Yes}

[ sections 4.{1,3} ]
Thank you for the clarifying examples at the boundary.

[ sections 4.2.2.{4,5,6} ]
What about non-{TCP,UDP} protocols with ports?  Should operators be able to
express a flow spec for SCTP traffic (e.g. within a 3GPP network context)?

There's no mention of any such support in 5575, so I'll presume this sort of
thing is left to another document (whether or not that document exists).

[ section 5.1 ]
My reading of this is that sorting between 2 different specifications each
with source and destinations prefixes must always be examined by destination
prefix first (because of the component ordering restriction in section 4.2).

Is this correct?

[ section 7 ]
"may interfere with each other even contradict"    -->
"may interfere with each other or even contradict" ?

[ section 11.2 ]
"by a an 8-bit" --> "by an 8-bit" ?

