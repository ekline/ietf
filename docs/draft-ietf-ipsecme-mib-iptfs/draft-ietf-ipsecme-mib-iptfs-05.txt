doc{draft-ietf-ipsecme-mib-iptfs-05}
ballot{No Objection}


[ document review ]

# Internet AD comments for {draft-ietf-ipsecme-mib-iptfs-05}
CC @ekline

## Comments

### S4.1, and elsewhere

* Tracking "incomplete" packets made me wonder: should there be a counter
  for number of packets fragmented?
