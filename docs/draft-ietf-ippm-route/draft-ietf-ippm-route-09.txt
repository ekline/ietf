doc{draft-ietf-ippm-route-09}
ballot{No Objection}

===
The comments below were all prompted handled by the authors.
===

ballot{Discuss}

[[ discuss ]]

[ section 4.1 ]

* I don't quite understand this apparent reliance on IPv6 flows being routed or
  load-balanced by src/dst and flow label alone.  I know of implementations
  where that is not the case at all, and in fact RFC 6437 specifically advises
  that the flow label alone not be relied upon.  Quoting section 2:

  o  Forwarding nodes such as routers and load distributors MUST NOT
     depend only on Flow Label values being uniformly distributed.  In
     any usage such as a hash key for load distribution, the Flow Label
     bits MUST be combined at least with bits from other sources within
     the packet, so as to produce a constant hash value for each flow
     and a suitable distribution of hash values across flows.
     Typically, the other fields used will be some or all components of
     the usual 5-tuple.  In this way, load distribution will still
     occur even if the Flow Label values are poorly distributed.

  Perhaps I'm seriously misunderstanding something though...

* In the bulleted list following the final paragraph, the IP identification
  field is an IPv4-only header field.  What is the recommended mechanism for
  IPv6?


[[ comments ]]

Overall, a very well written document, I thought; thank you.

[ section 3.3 ]

* Are N and Nmax actually determined at dst or are they determined by
  src when replies are received?

[ section 3.5 ]

* s/can't be detected at IP layer/can't be detected at the IP layer/

[ section 6 ]

* It looks like "parameter E" appears first in paragraph 5?  It might be good
  to provide some "E means exhaustive..." explanatory sentence.
