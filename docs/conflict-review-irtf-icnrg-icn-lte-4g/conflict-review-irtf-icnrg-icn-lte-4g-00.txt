doc{conflict-review-irtf-icnrg-icn-lte-4g-00}
ref{draft-irtf-icnrg-icn-lte-4g-11}
ballot{No Objection}


[ conflict review ]

* I have come to feel like ICN RG output tends to have some conceptual
  overlap with DTN WG work.  I defer on whether DTN should be listed
  as related work or not.
