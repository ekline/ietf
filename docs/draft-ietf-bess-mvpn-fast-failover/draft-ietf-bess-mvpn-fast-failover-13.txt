doc{draft-ietf-bess-mvpn-fast-failover-13}
ballot{No Objection}


[[ comments ]]

[ section 3.1.6.1 ]

* I too am not happy about seeing ::ffff:127.0.0.0/104 in the wild, but
  I personally think the community should have followed through on any of
  the various 1::/{32,48,64} proposals that could have resulted in IANA
  designating a loopback prefix.

  Maybe it's time to try taking up that work again.

  /me makes a note to self
