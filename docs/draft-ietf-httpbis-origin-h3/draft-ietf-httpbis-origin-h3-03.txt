doc{draft-ietf-httpbis-origin-h3-03}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-httpbis-origin-h3-03
CC @ekline

## Comments

### S1

* Is 9114 A.2.3 really where the required updates are discussed?  Maybe it's
  just much more subtle than I would have expected.

### S2

* Can an ORIGIN frame be sent from a child to the server on its control
  stream?  If not, what kind of error is it?
