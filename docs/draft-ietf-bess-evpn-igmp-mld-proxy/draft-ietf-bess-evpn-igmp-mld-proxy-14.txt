doc{draft-ietf-bess-evpn-igmp-mld-proxy-14}
ballot{No Objection}


[ document review ]

[[ comments ]]

Generally no useful comments that others haven't already made.  Thank
you for your patience, since I had pushed this out another week.

[S4, comment]

* I feel like some representative diagram to refer to throughout the
  document might be useful earlier in the document, even if it's just
  duplication of Figure 1 from section 5.

[S9.*]

* Should it be said that if the Multicast Source Length is not zero
  then it MUST be equal to the Multicast Group Length?  I.e. no
  mixing and matching IPv4 and IPv6 source/group addresses by accident?
