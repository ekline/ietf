doc{charter-ietf-masque-02-02}
ballot{No Objection}


[ document review ]

# Internet AD comments for charter-ietf-masque-02-02
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Nits

* Don't think it's necessary to get overly specific about IEEE 802.3.
  RFC 7241, and RFCs 7042 and its -bis, all just use the phrase "IEEE 802".
