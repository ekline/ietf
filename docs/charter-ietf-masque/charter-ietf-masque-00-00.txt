doc{charter-ietf-masque-00-00}

* 4th paragraph says "over MASQUE" without previously declaring the term
  MASQUE.  Suggest either to reword without MASQUE or add sentence declaring
  "all of the above proxying games are collectively called MASQUE", or
  something.
