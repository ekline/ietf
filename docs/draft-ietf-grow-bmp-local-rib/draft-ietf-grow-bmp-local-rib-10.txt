doc{draft-ietf-grow-bmp-local-rib-10}
ballot{No Objection}


[[ comments / questions / nits ]]

Mostly nits.

[ section 1.1 ]

* "directly effects" -> "directly affects"... I think

[ section 3 ]

* "refers to an instance of an instance of" ->  "refers to an instance of"?

[ section 4.1 ]

* "RD" has two expansions, according to the RFC Editor abbreviation list.
  Consider expanding on first use.

[ section 5, 6.1.3 ]

* "ie." -> "i.e.", I think

[ section 6.1.2 ]

* "that only IBGP and EBGP that should be sent" ->
  "that only IBGP and EBGP should be sent"

[ section 7 ]

* "SHOULD require to establish sessions" ->
  "SHOULD require establishing sessions", or something?

  Should these sessions also be required to be secure?
