doc{draft-ietf-lamps-rfc3709bis-07}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-lamps-rfc3709bis-07
CC @ekline

## Nits

### S1.2

* "from his wallet" -> "from their wallet", perhaps

### S6

* "replying party software" -> "relying party software"
