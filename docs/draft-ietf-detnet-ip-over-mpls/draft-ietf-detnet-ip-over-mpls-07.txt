doc{draft-ietf-detnet-ip-over-mpls-07}
ballot{No Objection}


[[ nits ]]

[ section 6 ]

* Perhaps "flow specific" -> "flow-specific"

* s/needed to provided/needed to provide/

* Perhaps "treatment needed to meet" -> "treatment to meet"
  (or s/needed/necessary/)
