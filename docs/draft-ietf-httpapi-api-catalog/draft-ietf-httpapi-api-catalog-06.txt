doc{draft-ietf-httpapi-api-catalog-06}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-httpapi-api-catalog-06
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Nits

### S5.1, S5.5

* "./well-known/api-catalog" ->
  "/.well-known/api-catalog" ?

* "https://www.example.net/./well-known/api-catalog" ->
  "https://www.example.net/.well-known/api-catalog" ?

I may have misunderstood if these are intentional, as opposed to simply
looking like typos...
