doc{draft-danyliw-replace-ftp-pointers-03}
ballot{No Objection}


[ document review ]

[[ comments ]]

[S3.11, nit]

* "March 002" -> "March 2002"

[S3.16, nit]

* Stray space still in the replacement URI?

