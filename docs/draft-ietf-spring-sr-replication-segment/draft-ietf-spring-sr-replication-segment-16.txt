doc{draft-ietf-spring-sr-replication-segment-16}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-spring-sr-replication-segment-16
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Comments

### S2.2.1

*   S01.   If (Upper-Layer header type == 4(IPv4) OR
               Upper-Layer header type == 4(IPv6) ) {

seems suspicious.  I think the second "4" should probably be "41"?

### S5

* ULAs are not "non-routable"; they're "non-globally-routable".

