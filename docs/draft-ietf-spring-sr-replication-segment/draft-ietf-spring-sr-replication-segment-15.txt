doc{draft-ietf-spring-sr-replication-segment-15}
ballot{Discuss}


[ document review ]

# Internet AD comments for draft-ietf-spring-sr-replication-segment-15
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Discuss

### S2.2.1

* I think there's some clarification required about what a Replication SID
  does if Segments Left is > 0.

  I can't see where Segments Left is decremented prior to packet duplication
  and so it looks like Replicate() would effectively H.Encaps[.Red] packets
  with an (inner) SRH that still points to the Replication SID?

* At S20, if Segments Left is still > 0 (even after whatever S16 is supposed
  to be doing), why would you discard all of the extension headers?

  I think the S14-S21 stuff is trying to say "if this Replication node is
  a leaf or a bud include it in the Replication List".  If that's true I
  suspect there are clearer ways to express that; you could just redefine
  the replication list inside Replicate() and let implementers figure out
  how to apply optimizations.

### A.2.1

* Please clarify which source address R6 uses to formulate the Echo Reply.

  I'm a little unclear on the mental model here.  The generation of ICMP
  errors is prohibited because the Replication SID is analogized to a
  multicast address in this context.

  Pinging a multicast address is of course fine, but the echo requester knows
  to expect the source address of replies to be any unicast address.  I'm
  assuming a requester needs to be modified to know that echo replies cannot
  come from the Replication SID in this case?

  Or is the Replication SID intended to be the source address of Echo Replies
  here, as if it's some kind of conceptual "unicast whenever we want but also
  multicast whenever say" kind of address?

## Comments

### S2/S2.2

* Given that Replicate() is implemented in terms of encapsulation in another
  SRH it's probably good to cite some text about the MTU considerations for
  operators.  Probably the usual "size your MTU big enough or expect trouble"
  type of advice is really all that can be said.

### S2/S6

* It seems like nothing in the control plane representation information can
  prevent a chain of replication SIDs forming a loop.  It should probably be
  noted that this can occur, looping and replicating packets until the
  Hop Limit stops it, if there is no function elsewhere that prevents the
  formation of loops when setting up the control plane (not necessarily a
  problem when a PCE is programming things, but in the "provisioned locally
  on a node" case it might be easier to make a mistake).

## Nits

### S1.1

* s/IPV6/IPv6/

### S2.2

* s/pen-ultimate/penultimate/
