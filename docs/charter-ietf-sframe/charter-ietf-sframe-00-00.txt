doc{charter-ietf-sframe-00-00}
ballot{No Objection}


[[ comments/questions ]]

* Agree with "Information to form a unique nonce within the scope of the key"
  seeming a bit underdefined at the moment.

* If conferencing migrates to carriage over QUIC, would that have any
  impact on/overlap with this work?

