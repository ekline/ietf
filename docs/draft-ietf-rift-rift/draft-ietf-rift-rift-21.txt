doc{draft-ietf-rift-rift-21}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-rift-rift-21
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Comments

### S3.2

* Figure 3 is excellent, thanks for that.

### S6.2, S6.3.1, S9.2

* TTL/HL of 1 is sometimes used with mulitcast communications, but I have
  found that 255 can be a stronger requirement (and one used in IPv6
  link-local unicast communications).

  Consider standardizing on just 255, and perhaps a add a reference in this
  section to RFC 5082.

  I see some some thought has been given to this in S9.2. Nevertheless,
  my two cents recommendation is to stick with just 255.

### S10.2

* Per RFC 5952 S4.3: "ff02::a1f7".

## Nits

### S1

* "information will be necessary to perform certain algorithms necessary"

  Consider de-dup'ing the "necessary".

### S3.1

* "A radix of a switch is number of switching ports"

  "A radix of a switch is the number of switching ports"

### S5.1

* _super_ nitty, but consider:

  "not flooded East-West or back South again"
  "not flooded East-West nor back South again"
