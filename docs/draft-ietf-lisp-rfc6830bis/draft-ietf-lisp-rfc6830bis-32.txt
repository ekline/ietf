doc{draft-ietf-lisp-rfc6830bis-32}
ballot{Discuss}


[[ discuss ]]

[ section 8 ]

* I think the currently architecture of IPv6 is such that at a minimum this
  doc should say that instances SHOULD NOT be used when the inner traffic is
  IPv6 as overlapping IPv6 prefixes are best and fairly easily avoided and
  folks should be encouraged to avoid recreating some of the limitations that
  were unavoidable in IPv4.

[ section 12 ]

* When the outer header is IPv6, the flow label may also be set a la RFC 6438.

* When the inner header is IPv6, the flow label may also be a factor in the
  hashing (6348, if the flow label is non-zero a la 6437).

[ section 16 ]

* Is it worth adding an extra warning about gleaning mappings for EIDs that
  the ETR would otherwise have routed internally via the IGP?

* In addition to basic uRPF, can an ETR do LISP-specific uRPF, i.e. look up
  the source EID in the mapping system and check that the source RLOC is within
  the set returned?  If so, the document might mention it.  If not, it might
  be good state explicitly that LISP does not afford this kind of uRPF check.


[[ comments ]]


[ section 4.1 ]

* Source host "host1.abc.example.com" is sending a packet to
  "host2.xyz.example.com", exactly what host1 would do if the site
  was not using LISP.

  Suggest:

  Source host "host1.abc.example.com" is sending a packet to
  "host2.xyz.example.com", exactly as it would if the site was not
  was not using LISP.

* (6) "Subsequent packets"?  Can you say here what happens to the first
  packet that caused the mapping lookup to happen?  Ah, I see it's in
  section 6...  Up to you if you want drop a forward reference here to that.

[ section 7.x ]

* Still another options is for the tunnel to generate fragments at the outer
  header layer.  Even though may not be standard or recommended practice, a
  few words saying why this shouldn't be considered seems good; recommend
  pointing to https://tools.ietf.org/html/rfc4459#section-3.1 for a summary of
  the badness.

[ section 9 ]

* "multiple RLOC" -> "multiple RLOCs"
