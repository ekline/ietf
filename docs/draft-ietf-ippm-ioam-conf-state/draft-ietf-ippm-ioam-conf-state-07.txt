doc{draft-ietf-ippm-ioam-conf-state-07}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-ippm-ioam-conf-state-07
CC @ekline

I support John's request for clarification.

## Comments

### S3.1

* "MUST send an echo reply without IOAM capabilities or no echo reply"

  I think I understand the intent, and overall I think this text is likely
  to not be an issue in practice.  But I'll note that anything "MUST" where
  ICMP is concerned may not exactly be enforceable.

## Nits

### S2.2

* Maybe consider adding DEX to this list (it's also fully expanded on first
  use in S1, but it might be nice for ease of reference).

### S3.1, S3.2

* /depends on the specific environment it is applied at/ ->
  /depends on the specific deployment environment/ maybe?
