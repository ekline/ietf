doc{draft-ietf-httpbis-client-hints-14}
ballot{No Objection}

[[ nits ]]

[ section 1 ]
* "requires external device" -> "requires an external device"?

[ section 2.2 ]
* "indicate user agents" -> "indicate to user agents"?

[ section 3.1 ]
* s/sh-list/sf-list/g

[ section 4.3 ]
* "we can remove" -> perhaps another wording that doesn't require "we"

* "unless the opt-in origin has explicitly delegated permission to another
  origin": is this delegation mechanism documented somewhere that should
  be referenced here?

* "is challenging and likely" -> "may be challenging or even"
