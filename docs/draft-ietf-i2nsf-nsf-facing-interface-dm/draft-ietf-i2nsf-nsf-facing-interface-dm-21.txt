doc{draft-ietf-i2nsf-nsf-facing-interface-dm-21}
ballot{No Objection}

[ document review ]

[[ comments ]]

[S5.1; nit]

* 2001:db8:0:1::0/120 should probably be 2001:db8:0:1::/120, which I think
  is more in keeping with RFC 5952 canonical string format.
