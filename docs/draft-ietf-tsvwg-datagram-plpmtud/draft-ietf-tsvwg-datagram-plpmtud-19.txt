doc{draft-ietf-tsvwg-datagram-plpmtud-19}
ballot{Yes}

[nits]

S1.1

* It's not clear to me that "stateful" in the phrase "stateful firewall"
  matters here. I imagine a "stateless" firewall could just as easily block
  incoming ICMP messages.

S4.5

* The "[RFC8201]" didn't render as a clickable link. Source XML weirdness?
  Same in S4.6.1.

S5.

* First sentence of second paragraph seems like two sentences, one sentence
  with a parenthetical comment, or a run-on sentence.  Also, I think "only be
  performed once" here means "only performed at one layer" rather than "only
  performed at one point in time"?

S5.1.1

* The "section 3.1.1" link seems to be internal to the draft rather than linked
  into RFC 8085, as I would expect from the text. (this happens twice in this
  section)

S5.3.3

* Is there any additional detail worth including here?  It asserts that a PL
  sender is able to detect inconsistencies, but I wonder whether more guidance
  (or an example) might be helpful to implementors.

S6.2.2

* s/is to be added//

S6.2.3.4

* Maybe check the XML for the "[RFC4960]" reference, since it doesn't seem to
  have been converted into a link.
