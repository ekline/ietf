doc{draft-ietf-anima-grasp-api-08}
ballot{No Objection}


[[ questions ]]

[ section 2.3.2.4 ]

* It looks like the URI may contain an IP address or FQDN as well as a
  port number?  If so, is there a validation requirement about the presence
  or value of the port field in the ASA_locator in relation to the port
  number in the URI?

[ section 2.3.3. ]

* For deregister_asa(), if the ASA name is redundant, does that mean that
  a call like deregister_asa(asa_nonce=valid_nonce, name="") should succeed?

  I suppose one ASA can deregister other ASAs by cycling through the 32-bit
  numberspace?

* For register_objective(), but happens if overlap=False for an objective
  already registered with overlap=True?  And what about the inverse?

  I guess, what is the trust model of multiple ASAs sharing a GRASP core
  (i.e. on the same node)?

[ section 2.3.4 ]

* For objectives that other ASAs on the same node might be trying to
  discover(), is the cache kept separate per-ASA or shared?

  If shared, it seems like the TTL<minTTL entries should be ignored and not
  deleted, maybe? (I haven't read any text describing cache implementation
  requirements or guidance yet.)

* For asynchronous mechanisms, is the callback (if used) called multiple
  times, as locators are discovered or are they accumulated until the timeout
  is reached and returned in one callback invocation?

  If the former, is there one final callback with, if necessary, an empty list
  to indicate the timeout was reached (as a convenience)?


[[ nits ]]

[ abstract ]

* "adapted to the support for" -> "adapted to add support for", perhaps

[ section 2.3.2.4 ]

* Perhaps replace "ifi...probably no use to a normal ASA" with something like
  "probably only of use to an ASA on a node with multiple active interfaces"?

[ section 2.3.6 ]

* s/caches all flooded objectives that it receive/... that it receives/
