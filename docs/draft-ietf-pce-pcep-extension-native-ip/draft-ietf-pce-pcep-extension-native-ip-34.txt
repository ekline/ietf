doc{draft-ietf-pce-pcep-extension-native-ip-34}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-pce-pcep-extension-native-ip-34
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Comments

### S6.4

* Why only IP-in-IP? Would it be possible/easy to cite the BGP Tunnel
  Encapsulation Attribute Tunnel Types IANA registry, plus whatever other
  mechanics/semantics that might go along with it?

  https://iana.org/assignments/bgp-tunnel-encapsulation/
