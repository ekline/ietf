doc{draft-ietf-detnet-tsn-vpn-over-mpls-06}
ballot{No Objection}


[[ comments ]]

* I know that RFC 8964 doesn't have any text about MTU and fragmentation
  considerations, but RFC 3985 does.

  The layering diagram in section 4.2 made me thing it might be worth
  either adding some text or pointing to some text elsewhere that
  advises the DetNet network operator to make sure that all the DetNet
  encapsulation overhead plus the MTU of the TSN not exceed the DetNet
  network's MTU, otherwise fragmentation considerations arise.

  Even a reference to RFC 3985 section 5.3 might be enough to remind the
  reader of these additional considerations.
