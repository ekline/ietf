doc{draft-ietf-taps-interface-22}
ballot{Discuss}


[ document review ]

# Internet AD comments for draft-ietf-taps-interface-22
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Discuss

### S6.1.12

* "there is currently no portable standard format for a PvD identifier"

  RFC 8801 should be considered here.

  An argument can be made that there is no *single* format, but there
  certainly is a string FQDN PvD ID standard.

  I think it's fair to require that the FQDN PvD ID strings be considered
  MTI here, or that an implementation have some way to use handles that
  refer to these.  Maybe that's what's meant here by the integer option
  mention and I'm just not getting the clue.

## Comments

### S3

* "The application should not assume that ignoring events..."
  s/should not/SHOULD NOT/?

### S6.1

* "Connection can perform name resolution"

  Does the Connection do name resolution?  I would have expected this to
  either be done by Preconnection or just "the API implementation".

### S6.1.3

* If calls like NewPreconnection() take an array of RemoteEndpoints why
  is it necessary to add one RemoteEndpoint to another via .AddAlias(),
  rather than just tossing into the []RemoteEndpoints array?

### S6.2

* It's probably too late for a bikeshed, but I find a Preference named
  "Ignore" to not mean "No Preference", as I read it.  I would have expected
  something like a Preference of "None".

  To me, "Ignore" feels kinda somewhat like "Avoid" (or "Eschew" :D ).

### S6.2.1

* "without corruption"?

  I think I would have expected "without loss"; "without corruption" implies
  to me that there's some extra level of integrity checking go on (vis.
  S6.2.7).

### S6.2.13

* Why should the preference be Avoid for Rendezvous use cases?  It seems
  to me like many Rendezvous uses are not necessarily long-lived and so
  might be Preference None (Ignore), or even Prefer.

### S9.1.3.7

* Same question as S6.2.1 above.

## Nits

### S5

* "Actions, events, and errors in implementations" ->
  "Action, event, and error objects in implementations"

### S6.1, S6.1.4

* "Port (a 16-bit integer)"

  Perhaps "unsigned integer", or "non-negative integer"?

* ":0a" -> ":a", I think

### S6.1.5

* "newPreconnection(...)" -> "NewPreconnection(...)"?

### S9.1.3.10

* s/endpount/endpoint/
