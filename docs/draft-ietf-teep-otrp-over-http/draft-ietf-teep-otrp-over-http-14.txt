doc{draft-ietf-teep-otrp-over-http-14}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-teep-otrp-over-http-14
CC @ekline

## Comments

### S5.6, S6.4

* Is there any discussion that can be referenced for how to set "reasonable"
  timeouts?  Even though the HTTP transport layer may be up and functioning,
  how long is too long to wait for message processing before an error should
  be declared by one of the layers?

  I'm sure the timeout needs might vary according to many different factors,
  but does the desired timeout need to be conveyed by some mechanism to the
  TEEP/HTTP {Client,Server} layer?

## Nits

### S7

* Item 1: s/cient/client/
