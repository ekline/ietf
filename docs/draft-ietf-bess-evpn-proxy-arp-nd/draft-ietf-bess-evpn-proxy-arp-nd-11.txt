doc{draft-ietf-bess-evpn-proxy-arp-nd-11}
ballot{Discuss}


[[ discuss ]]

[ meta ]

* I appreciate the attempt to explicitly discuss handling NS/NA messages with
  not-previously-seen options.  Thank you for that.

  It seems to me, however, that the proposed approach to proceed with
  setting the current capabilities in concrete but point to things like
  "unicast-forward" as a relief valve, even though 3.2-f seems to say the
  multicast packets (i.e. on cache miss) should be discarded (implying the
  unicast mapping might never be learned in the first place?).

[ general ]

* When a PE reboots, should it do MLD (e.g. 2710, 3810, ...) of some kind to
  gather critical state so that it doesn't have to wait for CEs to have
  problems?

[ section 3.1 ]

* To my knowledge there is no concept in IPv6 of a link where anycast/O=0
  NAs only propagate partway through a broadcast domain.  In practice, if I
  understand the architecture correctly, O=0 NAs will propagate to all CEs
  "behind" a given PE but, if anycast=false, no further.

  This could lead to a difficult to debug scenario (though I admit this is
  probably quite rare).

  Note that 4861-7.2.4 implies that most nodes sending NAs for their own
  addresses will adhere to "the Override flag SHOULD be set to one".  This
  is not a MUST, though.  Dropping all O=0 NAs might affect some
  implementations that don't comply with this SHOULD.  I have no idea how
  many implementations this might affect (in practice, I suspect none?), but...
  I think it might need to be considered.

[ section 3.1.1/3.2 ]

* I was not able to understand what the typical disposition of the O bit
  is supposed to be in these proxied NAs.  Is the intent to default to O=0 so
  that local O=1 can be preferred (vis. 4861-7.2.4 and 4389)?  Or will it
  more typically be set to O=1 (as if just replaying the NA that was learned
  by another PE)?

[ section 3.2 ]

* Item (f) as currently written would break Enhanced DAD (RFC 7527),
  would it not?

[ section 3.6 ]

* "Duplicate IP Detection for IPv6 SHOULD be disabled when IPv6
   'anycast' is activated in a given EVI."

  This doesn't seem like the right response to me.  It should be okay to keep
  doing DAD for O=1 addresses regardless of the setting of this 'anycast'
  option, I would have thought.

[ section 5.5 ]

* I think that recommending Reply Sub-Functions discard NS packets of
  unexpected for means, in practice, no new NS option or flag can ever really
  be made to work.  The PE(s) serving the CE(s) that make "new NS"
  solicitations will all need to be upgraded, and it's not immediately clear
  to me that remote PEs wouldn't also need to be upgraded (to support a
  possible "new NA"in response).

  If this is in fact likely to be the operational reality then I think this
  limitation probably needs to be noted explicitly.


[[ comments ]]

[ section 3.1.1 ]

* "                                   ... If no ARP/ND Extended
   Community is received, the PE will add a configured R Flag/O Flag
   to the entry.  This configured R Flag SHOULD be an administrative
   choice with a default value of 1."

   This reads as though the R flag should be added essentially by default?
   I realize that might seem reasonable on a broadcast domain populated by
   almost entirely by routers but it might cause confusion w.r.t. any
   hosts/servers added to the broadcast domain.  It seems like it might be
   better if the flags were always learned reliably, propagated reliably,
   and never presumed?

   I guess a host being mistaken for a router that never actually sends an
   RA doesn't cause any real problems in other nodes' ND tables.  I would
   think, though, that if R were presumed to be 0 then it would force R bit
   learning and propagation to be made to work reliably and be more easily
   detected if it doesn't.

[ section 5.6 ]

* Saying that anycast is not a requirement for IXPs seems like maybe a bit
  presumptuous speaking for all IXPs (maybe someone wants an anycast on-link
  NTP/PTP service?).  Perhaps just say that it's not /typically/ a requirement
  for IXPs?


[[ nits ]]

[ section 1 ]

* "BD: Broadcast Domain" is listed twice.

[ section 2.2 ]

* s/go worse/grow worse/, perhaps

[ section 3.1 ]

* s/to the all/to all/
