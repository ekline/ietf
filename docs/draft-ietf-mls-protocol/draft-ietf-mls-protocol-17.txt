doc{draft-ietf-mls-protocol-17}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-mls-protocol-17
CC @ekline

## Comments

* Thank you very much for all of the explicitly worked examples.

### S7.3

* What is the reason for MUST vs RECOMMENDED difference in the two different
  circumstances when validating the `lifetime` field?

* Consider adding a platitude about communicating nodes SHOULD use some
  method of time synchronization...

## Nits

### S3.2

* "top of these basic mechanism" -> "top of these basic mechanisms"
  (or maybe "top of this basic mechanism")

### S6.3.1

* "in an PrivateContentTBE structure" -> "in a PrivateContentTBE structure"
