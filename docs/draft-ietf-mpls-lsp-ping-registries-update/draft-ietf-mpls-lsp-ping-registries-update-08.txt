doc{draft-ietf-mpls-lsp-ping-registries-update-08}
ballot{No Objection}


[[ comments ]]

[ section 1.2.1 ]

* I find it slightly odd that this section even exists.

  RFC 8126 section 1 seems to provide a definition for "namespace" and
  for "assignment"/"registration".  My reading of 8126s1's definition
  of namespace doesn't seem to quite match this document's use of namespace
  (seems more like the definition of registry without the existence of
  a definition for "sub-registry"), but maybe I haven't spent enough time
  thinking about it.

  If IANA review is fine with this text here, I've no objection.


[[ nits ]]

[ section 1 ]

* "there hav been" -> "there have been"
