doc{draft-ietf-uuidrev-rfc4122bis-10}
ballot{Yes}


[ document review ]

# Internet AD comments for draft-ietf-uuidrev-rfc4122bis-10
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Nits

### S6.2

* "which is sorts" -> "which is sorted"?

### S6.10

* "UUIDs formats" -> "UUID formats"
