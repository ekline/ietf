doc{draft-ietf-core-resource-directory-25}
ballot{Discuss}

[[ discuss ]]

[ section 4.1.1 ]

* Did this get presented to 6man at any point, either via mail to the list or
  chair or in a presentation slot at an IETF meeting or a 6man interim?

  I feel confident that there would be no objection to the option as described
  here, but the working group should have its chance to make an evaluation
  irrespective of my opinion.

  ---

  If this is to be used when link-local methods don't work, another option
  would have been to add an RD PVD API key and recommend including a PVD
  option.

[ section 4.1.1 & 9.2 ]

* Please clarify which ND messages can carry an RDAO.  I suspect they should
  only appear in RAs, but it would be good to state the expectation explicitly.

[ Appendix A. ]

* Can you explain the ff35:30:2001:db8:1 construction?  RFC 3306 section 4
  defines some fine-grained structure, and I'm wondering how a group ID of 1
  is selected/computed/well known.  If there is already a COAP document
  describing this vis. RFC 3307 section 4.*, perhaps it's worth dropping a
  reference in here.



[[ comments ]]

[ section 1 ]

* I'm unclear on what "disperse networks" might mean.

[ section 10.1.1 ]

* What is meant by "therefore SLAAC addresses are assigned..." followed by this
  table of not-very-random-looking IPv6 addresses?

  Is the assumption that there might not be some off-network DNS server but
  there is some RA with a /64 A=1 PIO?
