doc{draft-ietf-lamps-cmp-algorithms-14}
ballot{No Objection}


[ document review ]

# Internet AD comments for {draft-ietf-lamps-cmp-algorithms-14}
CC @ekline

## Nits

### S2.2

* "as one-way hash function" -> "as a one-way hash function"?
