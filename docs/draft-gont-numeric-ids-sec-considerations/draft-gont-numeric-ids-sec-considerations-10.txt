doc{draft-gont-numeric-ids-sec-considerations-10}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-gont-numeric-ids-sec-considerations-10
CC @ekline

## Comments

### S4

* I don't quite see how implementations using flawed PRNGs is relevant
  here.  Such a failing is not really a protocol specification issue.

  (And we already have RFCs 4086, 8937, and others...)

## Nits

### S1

* "such properties not met" -> "such properties are not met"

### S3

* "or or an update to it" -> "or an update to it"

### S4

* "made made" -> "made"

* "of of" -> "of"

### S5

* "transientnumeric" -> "transient numeric"
