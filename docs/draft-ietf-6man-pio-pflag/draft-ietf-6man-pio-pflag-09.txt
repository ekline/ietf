doc{draft-ietf-6man-pio-pflag-09}


[ document review ]

# Internet AD comments for draft-ietf-6man-pio-pflag-09
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

## Comments

### S6.1

* I think the wording here is less clear than it might be regarding what
  activity is supposed to be stopped or continued:

  """
  *  When the length of the list decreases to zero, the host SHOULD
     stop requesting or renewing prefixes via DHCPv6 prefix delegation
     if it has no other reason to do so.
  """

  Perhaps something like:

  """
  *  When the length of the list decreases to zero, the host SHOULD
     stop requesting or renewing prefixes via DHCPv6 prefix delegation
     unless it has some other, unrelated reason to know that is should
     continue requesting or renewing.
  """

  Or something.

* I think this text is less clear than it might about which list of prefixes
  is observed for modifications:

  """
  *  If the host has already received delegated prefix(es) from one or
     more servers, then any time a prefix is added to or removed from
     the list, ...
  """

  Perhaps something like:

  """
  *  If the host has already received delegated prefix(es) from one or
     more servers, then any time a prefix is added to or removed from
     the list of prefixes with the P-flag set, ...
  """

### S6.2

* It might be better to be more explicit about when "prefix" refers to the
  PIO with the P-flag set and when it refers to a PD-delegated prefix.

  """
  *  The host MAY form as many IPv6 addresses from the [PD-delegated] prefix
     as it chooses.
  """

* Should we explicitly say that the re-advertisement of prefixes MUST NOT
  be done via the same interface over which the PD delegation was obtained?

  """
  *  The host MAY use the prefix to allow devices directly connected to
     it to obtain IPv6 addresses, e.g., by routing traffic for that
     prefix to the interface and sending a Router Advertisement
     containing the prefix on the interface.
  """

  This just says "the interface"; I think perhaps if it said "another
  interface" that might convey the requisite behaviour?  The next paragraph
  says not to forward dst addresses within the delegated prefix to the
  delegating interface, but it should also not even advertise, IMHO.

## Nits

### Abstract

* I-D nits says the doc should mention updating 4862 in the Abstract.

### S3

* "would be disruptive for applications that are using them" ->
  "would be disruptive for any applications that had begun using them"?

### S4

* "indicates that that the network" ->
  "indicates that the network" or "indicates that that network"
