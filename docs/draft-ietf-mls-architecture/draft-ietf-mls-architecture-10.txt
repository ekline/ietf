doc{draft-ietf-mls-architecture-10}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-mls-architecture-10
CC @ekline

## Nits

### S5.5

* "to other member" -> "to every other member"?

### S7.2.4

* "would have to be provide it" ->
  "would have to provide it", or
  "would have to be provided [with] it"
