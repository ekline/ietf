doc{conflict-review-irtf-icnrg-nrsarch-considerations-01}
ref{draft-irtf-icnrg-nrsarch-considerations-07}
ballot{Yes}


[ conflict review ]

The IESG has concluded that this work is related to IETF work done in
dnsop, dprive, dtn, intarea, and rtgwg, but this relationship does not
prevent publishing.
