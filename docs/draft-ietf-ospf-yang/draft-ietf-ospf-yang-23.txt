I am not deeply familiar with YANG/NETCONF. My reading of this 120+ page
doc is that probably proper validation can really be done by someone who
attempts to implement this (thereby discovered some possible mismatching
definition).

2.3: 'version' referenced, but does not appear in section 2.2. It seems
like it's only a property of an LSA.

Page 25: NMDA RFC is 8342, not 8242.

Page 81: references draft-ietf-bdf-yang-xx.txt. This is referenced
elsewhere in the doc (correctly), so I think just remove the -xx may
be fine.

Still some "RFC XXXX" to be filled in later, but that's to be expected.
