doc{draft-ietf-sedate-datetime-extended-10}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-sedate-datetime-extended-10
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Comments

### S2.2

* I concur with Lars' DISCUSS point.  Seems best to update or not update
  RFC 3339 section 4.3 ("there is no try" :-).

### S4.1

* Consider a sentence or two at the end that calls out the prohibition
  of "." and ".." from the time-zone-part production trailing comment.
  It seems to my uneducated eye that the ABNF actually allow this, so
  the checking would need to be done separately by implementations?

## Nits

### S1.2

* "UTC was designed to be a useful successor for" ->
  "for which UTC was designed to be a useful successor"

### S2.1

* "always was" -> "was always" or even just "has been"
