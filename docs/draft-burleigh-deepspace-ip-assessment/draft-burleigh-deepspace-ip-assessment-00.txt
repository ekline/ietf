doc{draft-burleigh-deepspace-ip-assessment-00}


[ document review ]

# comments for draft-burleigh-deepspace-ip-assessment-00
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

## Comments

### S3.1

* I believe I know what you mean by "static routes" but I'm not sure other
  folks would necessarily have the same reading.  I believe you're referring
  to routes provisioned and maintained by any control plane other than a
  dynamic routing protocol.

  I'm at a loss this afternoon to think of better terminology, but something
  along the lines of "manually provisioned", "controller provisioned", or
  something.  Hopefully something better will pop into my head later on.

### S3.2

* I think this section motivates nicely having a QUIC CLA for BP.  It also
  might equally imply that B and C running HTTP proxies could be a solution
  where the messages are, say, RFC 9292 or RFC 9110 HTTP messages.

  I'm not suggesting any text change; just an observation and a recollection
  of some discussion at STINT's 2nd day panel.

### S3.3

* I think it might be noted that this section applies even to the connections
  that might be formed for hop-by-hop store-and-forward transmission, for
  situations where the next-hop given in a DTN topology just happens to be
  really, really far away.

### S3.5

* Minor: Hop count and TTL are decremented, rather than incremented.

### S3.11

* Is the varying rate of time progression in different gravity fields also
  a concern when trying to obtain consensus on the current time?

  (I've asked this informally in NTP once or twice, but nobody has wanted to
  take up the consideration -- a reasonable thing to postpone, as far as
  terrestrial timekeeping is concerned).

## Nits

### S2

It's common nowadays to use the RFC 8174 Section 2 text to the same effect.
