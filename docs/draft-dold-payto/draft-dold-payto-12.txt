doc{draft-dold-payto-12}
ballot{No Objection}

[ section 5 ]
* How should an application know how large an instruction field to use? What
  happens to the transaction if the instruction field would be subject to
  lossy conversion?

[ section 7.1 ]
* "the routing number and the account number": is it slightly more clear to say
  "the routing number followed by the account number"?

  Same consideration in 7.3: "BIC and IBAN" -> "BIC followed by IBAN"
