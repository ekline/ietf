doc{draft-ietf-anima-autonomic-control-plane-28}
ballot{Discuss}


[[ discuss ]]

[ section 2 ]

* "It is the approximate IPv6 counterpart of the IPv4 private address
  ([RFC1918])."

  I understand the intent but I don't think this statement is complete. I
  think we shouldn't let this sentence out into the wild as is since it could
  be read without any context nor even any pretense of interest in nuance.

  May I suggest:

  "It is often thought of as the approximate IPv6 counterpart of the IPv4
  private address space ([RFC1918]), though it is in fact meaningfully
  different in important and subtle ways [and upon which this document relies]."

[ section 6.10.2 ]

* Please clarify the table at the end of this section.  It looks like only
  two Types are listed, but should all 4 bit values be present?  Where are the
  Z, F, and V bits in the address?

  I realize these are defined in the following sections, so it probably
  suffices to say something like "The Z, F, and V bits are allocated from
  within the sub-scheme portion of the address according to the following
  sections..." or something.

  Unassigned/unused Type values should maybe be listed in the table as
  "Reserved"?

[ section 8.1.3 ]

* Why is an RIO for ::/0 with a lifetime of 0 required?  Doesn't it suffice
  it set the default router lifetime to 0?  Separately, are all nodes required
  to be Type C?


[[ comments ]]

[ section 1 ]

* "as good as possible" might be "as well as possible", but I'm unsure if I
  have any grammatical basis for this (adverbial phrase vs adjectival phrase?).

* "ACPs functions" -> "ACP's functions"?

* "overview how" perhaps: "overview of how"

* "propriety extensions" -> "proprietary extensions"

[ section 2 ]

* "On virtual nodes their equivalent." isn't really a complete sentence.  I
  think if you just join it to the previous sentence with ";" it works
  grammatically.

[ section 6.1.1 ]

* s/devices "serialNumber"/device's "serialNumber"/

[ section 6.1.2 ]

* The example "fd89b714F3db00000200000064000000" contains an uppercase "F"
  and therefore doesn't conform to 32HEXLC, I believe.

* 2. 2.1, "a nodes ACP" -> "a node's ACP" (it is correct in the immediately
  preceding sentence).

[ section 6.1.3 ]

* The rsub field, even if deliberately not pertinent, is a bit conspicuous by
  its absence from this section I think. It might good to state so explicitly
  (at least I, as a reader, was expecting to see some mention of it).

[ section 6.1.5.1 ]

* I think the 32 hex lowercase IPv6 addresses in the examples are each missing
  a single hex character (31 instead of 32)?  Or maybe that's not what these
  fields are (or I've miscounted)?

[ section 6.3 ]

* "does not include ACP nodes ACP certificate" perhaps -> "does not include
  the ACP node's ACP certificate"

* With respect to DULL GRASP message fragmentation due to certificate
  inclusion: is it of any value to include the fingerprint of the ACP cert,
  for diagnostics...?

[ section 6.5 ]

* s/to easier distinguish/to more easily distinguish/

[ section 6.6 ]

* Feel free to phrase the backoff implementation in reference to RFC 8415
  section 15 semantics (I think: IRT = 10 seconds, MRT = 640 seconds).

[ section 6.7.3.2 ]

* Should Responder-IPv6-ACP-LL-addr be in the TSr set?

[ section 6.10.1 ]

* "not expected to be end-user host." --> "... hosts."

[ section 6.11.1.11 ]

* What does a manual configuration (6.10.4) advertise?  Just its /128?

* Where does the /96 come from?

[ section 6.12.3 ]

* "meant to be prioritize reliability" -> "meant to prioritize reliability"

[ section 6.12.5.1 ]

* s/adddress/address/

* (see Figure 15 --> (see Figure 15)

* on other type -> on other types

[ section 6.12.5.2.2 ]

* "ACP nodes MAY reduce the amount of link-local IPv6 multicast packets
   from ND by learning the IPv6 link-local neighbor address to ACP
   secure channel mapping from other messages such as the source address
   of IPv6 link-local multicast RPL messages - and therefore forego the
   need to send Neighbor Solicitation messages."

   Isn't this only true on links with configurations such that a node can trust
   that the source link layer address of the received RPL message is guaranteed
   to be that of the original sender?

[ Appendix A.1 ]

* Yeah, I gotta say I didn't understand why the acp-address scheme wouldn't
  support giving each ACP loopback its own /64 from the start.  That's
  14-16 bits of /64s in a single rsub ULA's /48 which would seem pretty simple
  to implement (a /64 per router in some deployments).
