doc{draft-ietf-grow-bmp-peer-up-04}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-grow-bmp-peer-up-04
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Nits

### S3

* "is is created" -> "is created"
