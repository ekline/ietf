doc{draft-ietf-cose-rfc8152bis-struct-10}
ballot{No Objection}

[[ nits ]]

[ section 1.6 ]

* "authentication check of the contents algorithm" ->
  "authentication check of the contents" ?

  How should I read this sentence about AE?  It seems to be copied from 8152,
  but I kinda feel like the 3rd (last) use of the word "algorithm" could be
  removed?

[ section 2 ]

* Table 1, CBOR tag 17: "COSE Mac w/o" -> "COSE MAC w/o"

[ section 5 ]

* "This is same" -> "This is the same"

* "That is the counter signature..." -> "That is, the counter signature..."

[ section 5.1 ]

* Isn't #6.98 for COSE_Sign_Tagged?  I think the CDDL fragment needs to fixed
  temporarily to:

    COSE_CounterSignature_Tagged = #TBD0(COSE_CounterSignature)

[ section 7 ]

* "of other than direct" -> "if other than direct"?

[ section 7.3 ]

* #2: "COSE_MAC" -> "COSE_Mac"?

[ section 9 ]

* "this new algorithms" -> "these new algorithms"

[ section 9.5.2 ]

* "the key from next layer down" -> "the key from the next layer down"
