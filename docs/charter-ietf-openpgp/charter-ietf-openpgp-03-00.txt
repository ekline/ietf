doc{charter-ietf-openpgp-03-00}
ballot{No Objection}


[ document review ]

# Internet AD comments for charter-ietf-openpgp-03-00
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

## Nits

### "Working Group Process"

* "For all work items will required demonstration of interoperable ..."

  didn't quite scan correctly for me.  Perhaps something like:

  "For all work items, demonstration of interoperability will be required..."
