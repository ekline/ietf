doc{draft-ietf-add-dnr-11}
ballot{Discuss}


[ document review ]

# Internet AD comments for {draft-ietf-add-dnr-11}
CC @ekline

## Discuss

This seem like exceptionally minor points, but are hopefully easily dispatched.

### S4.1, S5.1, S6.1

* The example ADN encoding in S4.1 shows that the trailing null byte is
  included in the encoding (the label length of the DNS root).

  This raises the question: why do any of these options need an explicit
  ADN length?  Given: the ADN is a mandatory message element, must be the
  first element in the message, and there can be no more than one such
  element, it seems that parsing bounded by the overall option length
  and validating the "RFC1035-ness" :-) of the span preceding the null byte
  might save a byte or two?

  (For comparison: the RFC 8801 PVD ID FQDN did not require a length hint.)

### S4.1, 6.1

* If an ADN length is to be retained in these messages, why is the ADN length
  2 bytes in the IPv6 variants whereas in the DHCPv4 option a 1 byte length
  suffices?  I know it seems silly to DISCUSS a 1 byte difference, but I
  figured it would be easy to either explain or fix.

* Similarly, why is the Addr Length 2 bytes?

  My reckoning of a 1-byte addr length would be the ability to list up to
  15 IPv6 addresses for a single ADN.  With 2 bytes a network can advertise
  ... over 4000 of them (for a single ADN)?

My suspicion is that the variable length nature of the ADN component means
these options are easily pushed out of 2/4/8 byte alignment, and there may
not be much benefit to attempting to adhere to something that only appears
like it might align well.

(Being parsimonious with bytes may be more of a concern for RAs than DHCP.)

## Comments

### S3.4

* I think it would be fair to note that RA options may be encapsulated within
  or associated with a PVD Option (RFC 8801) and that multihoming may be
  attempted by this mechanism, for client implementations with 8801 support.

### S7.1

* Consider rewording the active element doing the discarding/filtering:
  for sure "the router" may be doing the enforcement, but more broadly
  "the network infrastructure" might be doing the enforcement (e.g., at
  the switch, wireless access point, etc).

## Nits

### S7.4

* "an information" -> "any information", I think
