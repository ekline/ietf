doc{draft-ietf-jmap-quotas-10}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-jmap-quotas-10
CC @ekline

## Comments

### S6

* "to receive notifications" -> "to allow clients to receive notifications"

  Who/what is receiving the notifications seemed unclear to me. It read to
  me as though the server should support push so that the server could
  receive updates.  My assumption from a scan of 8620 S7 is that clients
  receive pushes.

## Nits

### S4.2

* "they judge changing frequently"

  Perhaps either "they judge to be changing frequently" or
  "they judge change frequently".
