doc{draft-ietf-raw-ldacs-10}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-raw-ldacs-10
CC @ekline

## Comments

### S1

* The use of RFCs 4291 and 7136 for "IPv6 based networking protocols" seems
  a bit odd to me.  These are addressing architecture and interface
  identifier documents; why not just RFC 8200 itself?

### S4, S7.3.3

* I'm sure 6MAN wg might be interested in hearing if anything special is
  required of IPv6 w.r.t. operating over the sub-IP layer here.

### S7.3.2

* Just an FYI: consider having a look at RFC 3366 ("Advice to link designers
  on link Automatic Repeat reQuest (ARQ)"), just in case there's anything
  helpful in there.  (To be clear: no action requested vis. this draft.)

### S9, S9.5.4

* I'm very glad to see that L2 integrity is a requirement.  Without it, many
  IPv6 on-link attacks become possible.

  However, even with L2 integrity, some consideration should be given to
  IPv6 operational security.  Please consider RFC 9099, along with several
  of the documents it references (RFC 4942, etc).

## Nits

### S3.2

* s/While the aircraft is on ground/While the aircraft is on the ground/?
