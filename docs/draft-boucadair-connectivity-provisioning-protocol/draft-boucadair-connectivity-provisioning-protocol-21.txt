doc{draft-boucadair-connectivity-provisioning-protocol-21}
response{no conflict}


[[ questions ]]

* Does the CANCEL message need a NONCE as well?


[[ nits ]]

[ section 1 ]

* "models and precises": I suspect "precises" isn't the correct word here.
  "identifies"?

* Section 6 is absent from the bulleted list in the document organization.
  In fact, this organization section seems slightly out of sync with the
  table of contents.

[ section 7 ]

* "certain the Provider to accept" -> "certain the Provider will accept"?

* RFC 7297 section 3.11 defines Activation Means (both capitalized),
  not section 3.1.

[ section 8.3 ]

* "tuplet" seems to be a musical term, and is probably fine here. But just
  in case you meant "tuple" I thought I'd call it out here.

* I'm confused: both the CAI and PAI are "used to identify the agreement..."?
  Surely on the PAI will be used, since the Provider's offer is what will be
  enacted, no?

  Ah, I infer from later on that each uses their own identifiers for tracking
  in their own databases, since from their perspective the other identifier is
  necessarily opaque.

[ section 8.6 ]

* The difference between CANCEL and WITHDRAW is not obvious at this point in
  the text.  I think WITHDRAW is "to withdraw a previously ACCEPTed agreement"?

  This is made clearer later.  Up to you if you want to add any text here.

[ section 9.2.5 ]
* "may include an information" -> "may include information" or maybe "may
  include an informational element" or something.

[ section 9.2.11 ]
* "Upon receipt of an UPDATE message" -> "Upon receipt of an ACTIVE message"?

