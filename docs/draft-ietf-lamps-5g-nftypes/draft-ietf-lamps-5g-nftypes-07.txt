doc{draft-ietf-lamps-5g-nftypes-07}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-lamps-5g-nftypes-07
CC @ekline

## Comments

### S3

* When there are more than one NFType elements in an NFTypes extension
  is there a RECOMMENDED (or even REQUIRED) ordering?
