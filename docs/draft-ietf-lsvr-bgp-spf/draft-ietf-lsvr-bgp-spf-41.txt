doc{draft-ietf-lsvr-bgp-spf-41}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-lsvr-bgp-spf-41
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Comments

### S6.2

* "is an implementation matter" ->
  "is an implementation and/or policy matter"  (just a suggestion)

## Nits

### S1.2

* "100s or 1000s prefixes" -> "100s or 1000s of prefixes"

* "verification of the whether or not to advertise" ->
  "verification of whether or not to advertise"

### S3

* "to insure backward compatibility" ->
  "to ensure backward compatibility"

### S4.2, S4.3

* "liveliness" -> "liveness" (RFC 5880 only speaks of "liveness")

### S5.2.2

* "When 0 is advertised and there parallel unnumbered links" ->
  "When 0 is advertised and there are parallel unnumbered links"

### S5.2.4

* s/insure/ensure/

  (in general throughout the doc, as well)
