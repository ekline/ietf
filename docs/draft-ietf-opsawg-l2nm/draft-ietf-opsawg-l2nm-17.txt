doc{draft-ietf-opsawg-l2nm-17}
ballot{No Objection}


[ document review ]

# Internet AD comments for {draft-ietf-opsawg-l2nm-17}
CC @ekline

## Comments

### S6

* Should the 'df-election' description make brief mention of the meaning
  and use of the 'revertive' boolean?

### S8.3

* I can find neither 'revertive' nor 'preempt' in RFC 8584.  Can the
  description be expanded a bit to provide more context (or maybe the
  reference section)?
