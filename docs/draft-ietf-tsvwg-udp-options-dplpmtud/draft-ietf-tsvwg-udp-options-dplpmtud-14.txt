doc{draft-ietf-tsvwg-udp-options-dplpmtud-14}
ballot{Yes}


[ document review ]

# Internet AD comments for draft-ietf-tsvwg-udp-options-dplpmtud-14
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Comments

### S4.2

* "At the receiver, a received probe packet that does not carry
   application data does not form a part of the end-to-end transport
   data and is not delivered to the Upper Layer protocol (i.e.,
   application or protocol layered above UDP)."

  It is theoretically possible, however, that a UDP-Options-aware
  implementation could "deliver" a zero-length payload notification to
  the application, I presume (a la RFC 8085 S5)?

  I suppose a similar question applies to S4.4.

## Nits

### S3

* "less than of equal to" ->
  "less than or equal to"

### S3.1

* "Reception of a RES Option by the sender" ->
  "Reception of a RES Option by the REQ sender"

  or something, as there are two "senders" involved.
