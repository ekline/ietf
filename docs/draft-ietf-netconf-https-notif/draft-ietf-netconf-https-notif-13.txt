doc{draft-ietf-netconf-https-notif-13}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-netconf-https-notif-13
CC @ekline

## Nits

### S4.1

* "SSE" acronym used without prior expansion.  Perhaps this is a well-known
  abbreviation within the relevant circles, but neither of the expansions from
  https://www.rfc-editor.org/materials/abbrev.expansion.txt seemed intuitively
  obvious (admittedly: I'm not *CONF/YANG specialist).

  Indeed, 8040 S1.1.5 suggests this means "Server-Sent Events", which is not
  currently in the RFC Editor's abbreviations list.
