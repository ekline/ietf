doc{draft-ietf-ipsecme-iptfs-17}
ballot{Discuss}

[ document review ]

# Internet AD comments for {draft-ietf-ipsecme-iptfs-17}
CC @ekline

## Discuss

### S6.1

* I think this document should get a separate protocol value from the IANA
  "Protocol Numbers" registry, since that's where 4303 S2.6 clearly says they
  values come from.

  The "value of 41 indicates IPv6" makes it pretty clear where this field
  gets its values from.

  https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
