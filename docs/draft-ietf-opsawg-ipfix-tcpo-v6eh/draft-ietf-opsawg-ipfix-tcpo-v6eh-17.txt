doc{draft-ietf-opsawg-ipfix-tcpo-v6eh-17}
ballot{No Objection}


[ document review ]

# Internet AD comments for draft-ietf-opsawg-ipfix-tcpo-v6eh-17
CC @ekline

* comment syntax:
  - https://github.com/mnot/ietf-comments/blob/main/format.md

* "Handling Ballot Positions":
  - https://ietf.org/about/groups/iesg/statements/handling-ballot-positions/

## Comments

### S3.6

* "reported that IPv6 packets with extension headers are often dropped"

  A useful citation here might be RFC 7872, "Observations on the Dropping of
  Packets with IPv6 Extension Headers in the Real World".

## Nits

### S3.4

* "the occurrences of the Fragment headers" ->
  "the occurrence of the Fragment header"

  to match the example scenario's description.
