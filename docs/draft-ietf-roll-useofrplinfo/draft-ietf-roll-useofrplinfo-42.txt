doc{draft-ietf-roll-useofrplinfo-42}
ballot{Discuss}


[[ discuss ]]

[ section 12 ]

* Ignoring an invalid RH3 header by the end host (I'm assuming this
  means that segments left > 0) doesn't specify whether the packet
  should be processed (ignore the RH) or the whole packet should be
  ignored.

  I might recommend instead referring to RFC 6554 S4.2 for how to handle
  RH3's if the node is also a RPL-aware router and say it MUST drop the
  packet if segments left is non-zero and it's not a RPL-aware router.

  Related: I'd also recommend:

  "It should just be noted that an incoming RH3 must be fully consumed, or
   very carefully inspected."

  ->

  "It should just be noted that an incoming RH3 MUST be fully consumed."


[[ comments ]]

[ section 8.1.3 ]

* I'm confused by the use of "consumed" here.  Is the final RH3 entry
  RUL's address?  I guess you could say RH penultimate hop "consumes"
  the header because the ultimate destination address is put in the
  header DA field.  Seems a bit odd though.

  I assume 6LR_n gets RUL's address from the last segment in RH3.

  "Consumed" means segments left == 0, I guess?  I suppose should have
  picked up on this terminology when it was first used in Section 2.
  Maybe clarify what it means in that section (2)?


[[ questions ]]

[ section 4.1.3 ]

* To be clear, the DODAG Configuration option flags being updated
  is the one in 6550 S6.7.6?

[ section 9 ]

* This the final paragraph strictly true?  It seems that in some situations
  in section 7 full-encapsulated packets can arrived at the RUL with all
  RPL artifacts removed.  Again: in certain situations.


[[ nits ]]

[ section 1.1 ]

* "uses cases" -> "use cases"

[ section 4.1.3 ]

* "when a node joins to a network will know" ->
  "when a node joins to a network it will know", I think

* "MUST be initialize to zero" -> "MUST be initialized to zero"

  (Separately: if this is quoting text from 6.7.6, then it's not
   exactly a direct quote.)

[ section 6 ]

* "while adding significant in the code" ->
  "while adding significant <noun|noun phrase> in the code", I think

[ section 9 ]

* "traffic originating from..." ->
  "Traffic originating from..."

[ section 12 ]

* "if attack" -> "if the attack" or "if an attack"
