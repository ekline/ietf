doc{charter-ietf-apn-00-01}
ballot{Abstain}


[ document review ]

# Internet AD comments for {charter-ietf-apn-00-01}
CC @ekline

## Comments

I'll Abstain on this round, as many others have made many fine points.  I did
think there was some improved text here, so thank you for that.

As I read this version, I mostly noticed the paragraph order and explanation
flow:

* The third paragraph seems like a much better explanation of goals,
  thank you.

* It seems like the 2nd and 4th paragraphs share quite a lot similarities.

* The 1st and 5th paragraphs seems somewhat related.

As such, I played around a bit with the structure (locally), and perhaps
these suggestions might be helpful for structure (or perhaps not).  I'd
suggest trying to the following exercise:

  [1] reordering the current paragraphs in the following order:

      3, 4, 2, 1, 5, 6, 7 (then Milestones...)

  [2] go through the paragraphs in this new order and join, trim, de-dup,
      and reflow text so that it progresses logically (i.e. doesn't jump
      around in the introduction and definition of terms and concepts)

  [3] consider ditching the "application-aware" name.  If you'd like to keep
      the APN initialism then perhaps something that still captures the
      goals of -01 paragraph 3 might be: "Augmented Performance Networking"
      ...or something.

## Nits

* "fine-granularity" might be changed to "fine-grained"
