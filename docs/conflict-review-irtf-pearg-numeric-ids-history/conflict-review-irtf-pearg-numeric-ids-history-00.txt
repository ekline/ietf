doc{conflict-review-irtf-pearg-numeric-ids-history-00}
ref{idraft-irtf-pearg-numeric-ids-history-10}
ballot{No Conflict}


[ conflict review ]

* The IESG has concluded that this work is related to IETF work done in WGs
  dnsop; ntp; 6man; and tcpm, but this relationship does not prevent
  publishing.



[ document review ]

# Internet AD comments for {draft-irtf-pearg-numeric-ids-history-10}
CC @ekline

## Discuss

## Comments

### S1, S4.4

* It's not clear that NTP refids belong in this list as currently described.
  Specifically, I don't think they're exactly "transient" in the same way
  these other examples are.

  But seeing as how you want to include the discussion of them vis.
  information leakage, I can't see any rewording that would be brief.

### S3

* You could just come out and explicitly say: on path attackers are excluded
  the threat model considered by this document (seems like there's a lot of
  text here that amounts to: we're not considering on path attackers).

## Nits

### S2

* "definitely distinguish" seems overkill  =)
  "distinguish" ought to suffice

### S4.3

* Everywhere an RFC is mentioned by number it doesn't seem like it adds
  anything to have "(formerly draft-foo)", but maybe that's for the
  RFC Editor to decide.

