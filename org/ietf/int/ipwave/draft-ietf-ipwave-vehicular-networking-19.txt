doc{draft-ietf-ipwave-vehicular-networking-19}


[ abstract, section 1 ]

* "lists up" -> "enumerates|sets forth|details|...", might be a better
  word/verb phrase


[ section 2 ]

* "table PC"? Perhaps "tablet PC", like in section 6.

[ section 3 ]

* There are two sentences in the final paragraph which both direct the reader
  to section 5 for vehicular IPv6 problem statement and requirements.
  Probably only one of those sentences is really required.

[ section 3.2 ]

* "can facilitates" -> "can facilitate"

* "can make the battery charging schedule" is technically correct, but the
  multiple possible interpretations of "make" in this context tripped me up
  looking for some final (indirect) object or something.

  Suggest something like "can plan the battery charging schedule", perhaps?

* "The existing IPv6 protocol must be augmented ..."

  Should there be some explanation about why this needs to be done at the IPv6
  layer and, more explicitly, why a Layer 2 solution is not an option that
  can also be considered?  I can understand that L2 options are out of scope
  for the IETF to work on, but are they also out of scope overall?  I could
  believe that the reason is RFC 4903 -style issues, but I figured I'd ask.

[ section 4.1 ]

* "...vehicles under the coverage of an RSU share a prefix such
   that mobile nodes share a prefix..."

   Perhaps s/such that/(just as)|(in the same way that)/?

[ section 4.2/5(.0) ]

* The final paragraph of 4.2 hints at this, but I found myself wanting to read
  about the expected "dwell times" for a vehicle connected to an IP-RSU.
  For how long is a vehicle expected to be connected to any given access node?
  I think the maximum is probably uninteresting (the vehicle could be parked,
  for example), but what is the useful minimum time?

  (I see that section 5.1.2 goes include a helpful timescale reference.)

[ section 5.1.1/5.2 ]

* During handover, can a vehicle be connected to multiple IP-RSUs on the
  logical interface?  If so, does this mean they need to use RFC 8028 -style
  address and next hop selection?

[ section 6 ]

* How can a vehicle authenticate other vehicles (and their ND information)
  and the RAs coming from IP-RSUs?

  Ah, I guess this is what the final two paragraphs are saying, actually, that
  there needs to be such a mechanism.
