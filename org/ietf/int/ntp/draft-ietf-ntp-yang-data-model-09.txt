doc{draft-ietf-ntp-yang-data-model-09}
ballot{Revised I-D Needed}

I have requested a YANG doctors review of -09 (last I saw was for -03).
Below are just a few questions and comments, but otherwise I think a
draft -10 is ready to go to IETF LC.


[[ questions ]]

[ section 7 ]

* Why does ntp-version default to "3" and not "4", given the references
  to RFC 5905?

  If the default does change from 3 to 4 then I think some of the examples
  may need updating (section 8.5 multicast client, for one).

* Can this YANG model be used to do all/most of things discussed in the
  mode 6 commands document?


[[ nits ]]

[ section 1 ]

* s/convers configuration/covers configuration/

* "parameters of per-interface" -> "per-interface parameters" ??

[ section 1.1 ]

* This sentence was a little unclear to me:

  "Additionally, the operational state also include the associations state."

  I may misunderstand, but I suggest:

  "The operational state includes NTP association state."

[ section 5 ]

* "allow such" -> "allows such"

[ section 6 ]

* "key-id is 32-bits unsigned integer" ->
  "key-id is a 32-bit unsigned integer"

[ section 7 ]

* "Enables the full access authority" -> "Enables full access authority"

* "Enables the server access and query" ->
  "Enables server access and control queries" perhaps?

* "Enables the server to access" -> "Enables basic server access" ?

[ section 8.1 ]

* s/2001:DB8::1/2001:db8::1/

  RFC 5952 section 4.3 specified lowercase hexadecimals.
