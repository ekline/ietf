doc{draft-ietf-lwig-minimal-esp-06}

Thanks for the changes from -05 to -06.

A few more nits, but I'm going to request IETF LC for -06.  All of this
can wait for other feedback from IETF LC and SEC-DIR review.

[S1] [comment]

* Thanks for cleaning up the requirements language issue.  I think this mean
  that RFCs 2119 and 8174 can be removed from the references section.

[S2] [nit]

* s/valueand/value and/
