doc{draft-ietf-6man-ipv6-alt-mark-05}


[S1] [nit]

* s/often referred as/often referred to as/

[S2] [question]

* "..., anyway it is to be expected that some routers cannot process it
  unless explicitly configured"

  doesn't strike me as grammatically correct and perhaps a run-on sentence.

  Is the intention to say that routers are expected to ignore the option
  unless configured (i.e. for performance reasons)?  If so, consider
  splitting this paragraph into two sentences, with the 2nd being something
  like:

  ".  It is expected that routers will ignore this option unless explicitly
  configured to process it." (something similar to the text in section 4)

[S2] [question]

* ", and associate different uses." seems a bit unclear to me.  Does mean
  something like "are intended for different use cases."?

[S2] [nit]

* s/the the uniqueness/the uniqueness/

[S2] [discuss]

* "and this could also happen to the IP addresses that can change due to NAT"
  (and in the subsequent sentence)

  How did IPv6 NAT find its way into this discussion?  Unless it is somehow
  critical to open up this discussion, I'd recommend just removing mention
  of it.

[S4] [nit]

* "by every node that is an identity in the SR path"

  I'm sure "identity" is the best word here.  Perhaps "that is identified
  by the SR path"?

[S4] [question]

* The intended meaning of the final paragraph of this section eludes me.
  What is an example of a "non-conventional application" of a Dest option
  that has a similar effect to an HbH option?

[S6] [question]

* Can you explain how "network reconnaissance through passive eavesdropping
  on data-plane traffic does not allow attackers to gain information about
  the network performance"?  It seems to me that an eavesdropping observer
  would have all the same information that src and dst do for inferring
  performance...yes?  (In the case of an HbH option, each on-path router is
  indistinguishable from an eavesdropping observer?)

[S7] [nit]

* "assignments" -> "assignment", I think, since it appears the doc is only
  requesting a single assignment from a single registry.
