# AD Dashboard

- [datatracker](https://datatracker.ietf.org)
- [new AD info](https://trac.ietf.org/trac/iesg/wiki/newADinfo)

## working groups

[**all**](https://datatracker.ietf.org/wg/)

mine:
- [6man](https://datatracker.ietf.org/wg/6man)
- [bpf](https://datatracker.ietf.org/wg/bpf)
- [dmm](https://datatracker.ietf.org/wg/dmm)
- [dtn](https://datatracker.ietf.org/wg/dtn)
- [ntp](https://datatracker.ietf.org/wg/ntp)
- [tictoc](https://datatracker.ietf.org/wg/tictoc)

closed:
- [6tisch](https://datatracker.ietf.org/wg/6tisch)
- [ipwave](https://datatracker.ietf.org/wg/ipwave)
- [lwig](https://datatracker.ietf.org/wg/lwig)

## regular regimen

### documents

1. [my documents](https://datatracker.ietf.org/doc/ad/erik.kline)
1. [next telechat : documents](https://datatracker.ietf.org/iesg/agenda/documents/)
1. [documents in last call](https://datatracker.ietf.org/doc/in-last-call)

### directorates

1. iot-dir [datatracker](https://datatracker.ietf.org/group/iotdir/reviews/) | [wiki](https://trac.ietf.org/trac/int/wiki/IOTDirWiki)

### milestones

1. [milestone review (all)](https://datatracker.ietf.org/iesg/milestones/)

### meetings

1. [next telechat : agenda](https://datatracker.ietf.org/iesg/agenda/)
1. [upcoming meetings](https://datatracker.ietf.org/meeting/upcoming)

### errata

1. [all INT area "reported" errata](https://rfc-editor.org/errata_search.php?rec_status=2&area_acronym=int&presentation=table)
1. [how to verify](https://www.rfc-editor.org/how-to-verify/)
1. [Status and Type of RFC Errata](https://www.rfc-editor.org/errata-definitions/)
1. ["IESG Processing of RFC Errata for the IETF Stream"](https://www.ietf.org/about/groups/iesg/statements/processing-errata-ietf-stream/)

### other process matters

1. review IANA action items

## IESG
- [IESG public wiki](https://trac.ietf.org/trac/iesg/wiki)
- [IESG charter](https://rfc-editor.org/rfc/rfc3710) (RFC 3710)
- [IESG ballot procedures](https://ietf.org/standards/process/iesg-ballots/)
- [IESG DISCUSS criteria](https://ietf.org/about/groups/iesg/statements/iesg-discuss-criteria/)
- [IESG document states](https://datatracker.ietf.org/help/state/draft/iesg)
- [consensus required for IETF stream](https://www.rfc-editor.org/rfc/rfc8789)
- [AD sponsoring](https://ietf.org/about/groups/iesg/statements/area-director-sponsoring-documents/)
- [RFC 5742](https://rfc-editor.org/rfc/rfc5742) "IESG Procedures for Handling of Independent and IRTF Stream Submissions"

### AD review

- [checklist](https://trac.ietf.org/trac/iesg/wiki/ADReviewChecklist)
  1. Formality
  1. Last Call related
  1. Content
     1. updates, ID nits, ...
     1. technical content
     1. [expert topics](https://trac.ietf.org/trac/iesg/wiki/ExpertTopics)

### charters

1. chartering [new](https://trac.ietf.org/trac/iesg/wiki/CharteringWorkingGroups) working groups
1. rechartering [existing](https://trac.ietf.org/trac/iesg/wiki/CharteringOntoAgenda) working groups

### bofs

1. IETF "How We Work" on [BoFs](https://www.ietf.org/how/bofs/)
1. [RFC 2418 §2.4](https://www.rfc-editor.org/rfc/rfc2418.html#section-2.4) BoF section in "IETF Working Group Guidelines and Procedures"
1. [IESG BoF wiki](https://trac.tools.ietf.org/bof/trac/wiki)
1. [RFC 5434](https://www.rfc-editor.org/rfc/rfc5434) "Considerations for Having a Successful Birds-of-a-Feather (BOF) Session"
1. [RFC 6771](https://www.rfc-editor.org/rfc/rfc6771) "Considerations for Having a Successful "Bar BOF" Side Meeting"
1. [IAB note on BoFs](https://www.iab.org/documents/correspondence-reports-documents/2012-2/iab-member-roles-in-evaluating-new-work-proposals/)

### conflict reviews

1. [IESG wiki](https://trac.ietf.org/trac/iesg/wiki/IesgRfceditorSubmissions)
1. [RFC 5742](https://www.rfc-editor.org/rfc/rfc5742) "IESG Procedures for Handling of Independent and IRTF Stream Submissions"

## IANA

### IANA allocations

1. [RFC 8126](https://www.rfc-editor.org/rfc/rfc8126.html)
1. [RFC 7120](https://www.rfc-editor.org/rfc/rfc7120.html) (early allocations)
1. general IANA registration [help](https://www.iana.org/help/protocol-registration)
1. [Memorandum](https://www.icann.org/en/system/files/files/ietf-iana-agreement-2024-13dec23-en.pdf) between IETF LLC and ICANN

## IRTF

### Guidelines

1. [RFC 2014](https://rfc-editor.org/rfc/rfc2014) "IRTF Research Group Guidelines and Procedures"
1. [RFC 4440](https://rfc-editor.org/rfc/rfc4440) "IAB Thoughts on the Role of the Internet Research Task Force (IRTF)"
1. [RFC 7418](https://rfc-editor.org/rfc/rfc7418) "An IRTF Primer for IETF Participants"

----

## RFC process

### standards process

1. [IETF Note Well](https://ietf.org/about/note-well/)
1. standards process:
   1. [IESG sumary](https://ietf.org/standards/process/)
   1. [RFC 2026](https://rfc-editor.org/rfc/rfc2026)
   1. [standards track maturity levels](https://rfc-editor.org/rfc/rfc6410) (RFC 6410)
   1. [IESG Ballot Procedures](https://www.ietf.org/standards/process/iesg-ballots/)
   1. [Handling IESG ballot positions](https://www.ietf.org/blog/handling-iesg-ballot-positions/)
1. consensus:
   1. [RFC 7282](https://www.rfc-editor.org/rfc/rfc7282)
      "On Consensus and Humming in the IETF"
   1. [RFC 8789](https://www.rfc-editor.org/rfc/rfc8789)
      "IETF Stream Documents Require IETF Rough Consensus"
1. [chairs resources](https://ietf.org/chairs/)
1. [working group guidelines](https://rfc-editor.org/rfc/rfc2418)
1. document adoption:
   1. [RFC 7221](https://rfc-editor.org/rfc/rfc7221)
   1. [right to produce derivative work](https://rfc-editor.org/rfc/rfc5378#section-3.3) (result of adoption)
1. [working group mailing list management](https://rfc-editor.org/rfc/rfc3934)
1. [document shepherding guidelines](https://rfc-editor.org/rfc/rfc4858)
1. [anti-harassment procedures](https://rfc-editor.org/rfc/rfc7776)
1. [AUTH48 AD approval for authors](https://www.ietf.org/about/groups/iesg/statements/auth48/) IESG statement on ack'ing for slow/missing authors
1. Working with `git`:
   1. [RFC 8874](https://datatracker.ietf.org/doc/html/rfc8874) "Working Group GitHub Usage Guidance"
   1. [RFC 8875](https://datatracker.ietf.org/doc/html/rfc8875) "Working Group GitHub Administration"

### rfc editor

1. [RFC 8729](https://www.rfc-editor.org/rfc/rfc8729.html) "The RFC Series and RFC Editor"

### IETF RFC Technical Considerations
- [techrefs/ietf_rfc_considerations.md](
  https://gitlab.com/ekline/techrefs/-/blob/main/ietf_rfc_considerations.md)
